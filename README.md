JRuby Example Plugin
====================

This is an example Alassian plugin using JRuby and Java.

The plugin shows:

* How to embed the JRuby runtime (via the JRuby Embed API),
* How to execute Ruby scripts that are part of the plugin
* How to set variables from Java to be used in Ruby scripts
* How to work with the output of Ruby scripts
* How to automatically install and bundle Ruby libraries (RubyGems)


![Working with Ruby objects in Java](http://ssaasen.bitbucket.org/atlassian-jruby-example-plugin/images/jruby-example-2.png)

![Ruby Sinatra app running in Confluence](http://ssaasen.bitbucket.org/atlassian-jruby-example-plugin/images/connie-sinatra-3.png)


---

*This plugin is for demonstration purposes. Hence it is incomplete and not
production ready.*

---



Development
===========

Prerequisite:

Make sure that you have installed the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Installing+the+Atlassian+Plugin+SDK) and that the various `atlas-*` commands are available on your `PATH`.


Clone the repository and start Confluence from within the plugin project

    atlas-debug

The first time this command is executed, the required RubyGem libraries will be
automatically downloaded and copied to the `src/main/ruby/rubygems` directory.

The Maven project is set up to copy the Ruby scripts in `src/main/ruby/**` into
your plugin.

The list of RubyGems is defined in the `required.gems` property. Feel free to
change this list to add the RubyGems you need for your own implementation.

    <required.gems>sinatra jruby-rack rio nokogiri</required.gems>

After updating the list, the new RubyGems can be downloaded by running:

    mvn -Prubygems process-resources

Note: The build process creates an index file `META-INF/gemspec.index` that is used
by the `ScriptingContainerProviderImpl` to correctly set the Ruby `LOAD_PATH`
so that all the RubyGems can be read from the classpath.


Running Tests
=============

At the moment the Ruby tests need to be run manually (they are not executed as
part of the Maven build yet).

With `jruby` available on your `PATH` run (see http://jruby.org/getting-started to get started):

    jruby -S bundle install
    jruby -S rake test

References
==========

https://github.com/jruby/jruby/wiki/JRubyCompiler

https://github.com/jruby/jruby/wiki/FAQs#Calling_Into_Java

https://github.com/jruby/jruby/wiki/GeneratingJavaClasses

http://www.sinatrarb.com/
