package com.atlassian.plugins.polyglot.jrubyexample.bridge;

import static com.atlassian.plugins.polyglot.jrubyexample.bridge.ScriptingContainerProviderImpl.ResourcePathProvider;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ScriptingContainerProviderTest {

    private static final String EXAMPLE_PATH = "src/main/ruby,src/main/rubygems/gems,src/main/resources";
    
    @Test
    public void testGetScriptingContainer() throws Exception {

    }


    @Test
    public void testLocalResourceProviderEmptyString() {
        ResourcePathProvider resourcePathProvider = new ResourcePathProvider("");
        assertNull(resourcePathProvider.getRubyGemsSourcesPath());
        assertNull(resourcePathProvider.getRubySourcesPath());
    }

    @Test
    public void testLocalResourceProviderGemsPath() {
        ResourcePathProvider resourcePathProvider = new ResourcePathProvider(EXAMPLE_PATH);
        assertTrue(resourcePathProvider.getRubyGemsSourcesPath().endsWith("src/main/rubygems/gems"));
    }

    @Test
    public void testLocalResourceProviderSourcePath() {
        ResourcePathProvider resourcePathProvider = new ResourcePathProvider(EXAMPLE_PATH);
        assertTrue(resourcePathProvider.getRubySourcesPath().endsWith("src/main/ruby"));
    }

}
