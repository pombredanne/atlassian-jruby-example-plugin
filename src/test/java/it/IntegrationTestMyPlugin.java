package it;

public class IntegrationTestMyPlugin extends AbstractIntegrationTestCase {
    public void testSinatraAppDashboard() {
        gotoPage("/plugins/servlet/sinatra/");
        assertTextPresent("Dashboard");
    }

    public void testRurl() {
        gotoPage("/plugins/servlet/jruby");
        assertTextPresent("Make an HTTP Request");
    }
}
