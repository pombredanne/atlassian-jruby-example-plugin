require File.join(File.dirname(__FILE__), 'ts_helper')
require 'app/confluence_helper'

class ClassUnderTest
  include Confluence::Helper
end

class ConfluenceSinatraAppTest < Test::Unit::TestCase
  context "Confluence Sinatra App " do
    setup do
      @app = ClassUnderTest.new
    end

    should "replace links in content bodies" do
      assert_equal '<a href="/confluence/plugins/servlet/sinatra/wiki/ds/Using+the+menus">Using menues</a>', @app.replace_links('<a href="/confluence/display/ds/Using+the+menus">Using menues</a>', '/display', '/plugins/servlet/sinatra/wiki')
    end

    should "replace links in content bodies ignoring attribute order" do
      assert_equal '<a class="test" href="/confluence/plugins/servlet/sinatra/wiki/ds/Using+the+menus">Using menues</a>', @app.replace_links('<a class="test" href="/confluence/display/ds/Using+the+menus">Using menues</a>', '/display', '/plugins/servlet/sinatra/wiki')
    end

  end
end
