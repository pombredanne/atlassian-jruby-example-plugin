# Bridge to lookup components from the container manager
# of the host application.
class HostApplicationComponentManager

  # Dynamically generate methods that look up a component by name.
  #
  # Usage: HostApplicationComponentManager.page_manager -> Return the bean
  # named "pageManager".
  # Add to this list to expose other components
  %w(page_manager space_manager permission_manager view_renderer).each do |meth|
    (class << self; self; end).instance_eval do
      define_method meth do |*args|
        Java::ComAtlassianSpringContainer::ContainerManager.get_component(bean_name(meth.to_s))
      end
    end
  end

  # Class helper methods
  class << self
    def classify(str)
      str.gsub(/(^|_)(.)/) { $2.upcase }
    end

    def lower_case_first(str)
      str[0, 1].downcase + str[1..-1]
    end

    def bean_name(str)
      lower_case_first(classify(str))
    end
  end

end
