require 'rubygems'
require 'rack'
require 'jruby-rack'
require 'rack/handler/servlet'
require 'jruby_integration/host_application_container_manager'
require 'app/confluence_sinatra_app'

rack_app = Rack::Builder.app do
 run ConfluenceSinatraApp
end

# The last expression will be returned
Rack::Handler::Servlet.new rack_app
