@request.instance_variable_set(:@context, @config.getServletContext)

class << @request
  def to_io
    self.getInputStream.to_io
  end
  def getScriptName
    self.getPathTranslated
  end
  def context
    @context
  end
end
@handler.call(@request)
