require 'sinatra/base'
require 'app/confluence_helper'

Permission = com.atlassian.confluence.security.Permission

class PermissionException < StandardError; end

class ConfluenceSinatraApp < Sinatra::Base

  include Confluence::Helper

  configure do
    enable :logging
    disable :show_exceptions
  end

  before do
    @current_user = current_user
    redirect "#{$context_path}/login.action" unless @current_user
  end

  not_found { erb :not_found }

  error PermissionException do
    @message = env['sinatra.error'].message
    erb :error
  end

  get '/' do
    @spaces = HostApplicationComponentManager.space_manager.get_all_spaces
    erb :index
  end

  get '/wiki/:space/:title' do
    title, space = unescape(params[:title]), params[:space]
    @page = page_manager.get_page(space, title)
    raise Sinatra::NotFound unless @page
    raise PermissionException, "You are not allowed to view this page" unless current_user_has_view_permission(@page)
    @body = replace_links(view_renderer.render(@page), '/display', '/plugins/servlet/sinatra/wiki')
    erb :show_page
  end

  # Expose methods like "escape" so that they can be used in templates
  helpers { include Rack::Utils }

  private
  def current_user_has_view_permission(obj)
    HostApplicationComponentManager.permission_manager.has_permission(current_user, Permission::VIEW, obj)
  end

  def current_user
    Java::ComAtlassianConfluenceUser::AuthenticatedUserThreadLocal.get_user
  end

  def view_renderer
    HostApplicationComponentManager.view_renderer
  end

  def page_manager
    HostApplicationComponentManager.page_manager
  end

end
