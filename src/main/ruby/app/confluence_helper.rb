module Confluence
  module Helper
    def replace_links(html, from, to)
      html.gsub %r{<a(.+)href="([^"]+)"} do |match|
        "<a#{$1}href=\"#{$2.gsub(from, to)}\""
      end
    end

  end
end
