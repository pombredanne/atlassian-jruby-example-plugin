package com.atlassian.plugins.polyglot.jrubyexample.servlet;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugins.polyglot.jrubyexample.bridge.ScriptingContainerProvider;
import org.apache.commons.lang.StringUtils;
import org.jruby.embed.*;
import org.jruby.exceptions.RaiseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.security.Principal;
import java.util.Collections;
import java.util.Map;

public class SinatraAdapterServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(SinatraAdapterServlet.class);

    private ServletConfig servletConfig;
    private EmbedEvalUnit rackServletHandler;
    private ScriptingContainer container;
    private String servletName = "";
    private String webResourceTags = "";
    private final WebResourceManager webResourceManager;
    private final ScriptingContainerProvider containerProvider;

    @Autowired
    public SinatraAdapterServlet(ScriptingContainerProvider containerProvider, @SuppressWarnings("SpringJavaAutowiringInspection") WebResourceManager webResourceManager) {
        this.containerProvider = containerProvider;
        this.webResourceManager = webResourceManager;
    }

    @Override
    public void init(ServletConfig config) {
        this.servletConfig = config;
        servletName = StringUtils.defaultString(config.getInitParameter("servletName"), "");

        container = containerProvider.getScriptingContainer();
        container.runScriptlet("require 'rubygems'");

        // parse closes the InputStream!
        rackServletHandler = container.parse(PathType.CLASSPATH, "/jruby_integration/rack_servlet_handler.rb");

        Writer sw = new StringWriter();
        webResourceManager.includeResources(Collections.singletonList("com.atlassian.example.jrubyplugin:jruby-plugin-resources"), sw, UrlMode.AUTO);
        webResourceTags = sw.toString();
    }


    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            final Writer w = response.getWriter();
            container.setOutput(w);

            container.put("$resource_tags", webResourceTags);
            container.put("$context_path", request.getContextPath());
            container.put("$servlet_context", servletConfig.getServletContext());

            container.put("@config", servletConfig);
            container.put("@handler", rackServletHandler.run());
            container.put("@request", new HttpServletRequestWrapper(request) {
                @Override
                public String getPathInfo() {
                    return super.getPathInfo().replaceFirst(servletName, "").replaceFirst("//", "");
                }
            });

            Object rackResponse = container.runScriptlet(PathType.CLASSPATH,
                    "/jruby_integration/handle_request.rb");

            String body = container.callMethod(rackResponse, "getBody", String.class);
            Integer status = container.callMethod(rackResponse, "getStatus", Integer.class);
            Map<String,Object> headers = container.callMethod(rackResponse, "getHeaders", Map.class);
            for(Map.Entry<String,Object> e : headers.entrySet()) {
                response.setHeader(e.getKey(), e.getValue().toString());
            }
            response.setStatus(status);
            response.setContentLength(body.getBytes().length);
            w.write(body);
            container.clear();

        } catch (RaiseException re) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, re.getLocalizedMessage());
        } catch (EvalFailedException efe) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, efe.getLocalizedMessage());
        } catch (InvokeFailedException ife) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ife.getLocalizedMessage());
        }
    }

}
