package com.atlassian.plugins.polyglot.jrubyexample.servlet;

import com.atlassian.confluence.core.ConfluenceSystemProperties;
import com.atlassian.plugins.polyglot.jrubyexample.bridge.ScriptingContainerProvider;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang.StringUtils;
import org.jruby.embed.EvalFailedException;
import org.jruby.embed.InvokeFailedException;
import org.jruby.embed.PathType;
import org.jruby.embed.ScriptingContainer;
import org.jruby.exceptions.RaiseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple Servlet that shows how to execute a Ruby script.
 */
public class ExampleRubyServlet extends HttpServlet {

    private final ScriptingContainerProvider scriptingContainerProvider;
    private final TemplateRenderer templateRenderer;

    public ExampleRubyServlet(ScriptingContainerProvider scriptingContainerProvider, TemplateRenderer templateRenderer) {
        this.scriptingContainerProvider = scriptingContainerProvider;
        this.templateRenderer = templateRenderer;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        final String url = req.getParameter("url");

        Map<String, Object> ctx = new HashMap<String, Object>();

        if (StringUtils.isNotBlank(url)) {
            try {
                ScriptingContainer container = scriptingContainerProvider.getScriptingContainer();
                if(ConfluenceSystemProperties.isDevMode()) {
                    container.runScriptlet("load 'rurl.rb'");
                } else {
                    container.runScriptlet("require 'rurl'");
                }

                Object rurl = container.runScriptlet("Rurl.new");
                HttpResponseInformation responseInformation = (HttpResponseInformation) container.callMethod(rurl, "curl", url);
                ctx.put("responseInformation", responseInformation);
                ctx.put("url", url);
            } catch (RaiseException re) {
                ctx.put("error", re.getLocalizedMessage());
            } catch (EvalFailedException efe) {
                ctx.put("error", efe.getLocalizedMessage());
            } catch (InvokeFailedException ife) {
                ctx.put("error", ife.getLocalizedMessage());
            }
        }
        templateRenderer.render("/templates/rurl.vm", ctx, resp.getWriter());
    }

}
