package com.atlassian.plugins.polyglot.jrubyexample.bridge;

import org.jruby.embed.ScriptingContainer;

public interface ScriptingContainerProvider
{
    ScriptingContainer getScriptingContainer();
}
