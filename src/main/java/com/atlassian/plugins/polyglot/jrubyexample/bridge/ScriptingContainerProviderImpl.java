package com.atlassian.plugins.polyglot.jrubyexample.bridge;

import static org.jruby.RubyInstanceConfig.CompileMode;

import com.atlassian.confluence.core.ConfluenceSystemProperties;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.util.concurrent.Supplier;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jruby.embed.LocalContextScope;
import org.jruby.embed.LocalVariableBehavior;
import org.jruby.embed.ScriptingContainer;
import org.jruby.util.KCode;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class ScriptingContainerProviderImpl implements ScriptingContainerProvider, DisposableBean {

    private final Supplier<ScriptingContainer> engine = new LazyReference<ScriptingContainer>() {
        @Override
        protected ScriptingContainer create() throws Exception {
            // Unfortunately we can't use the more suitable OSGiScriptingContainer
            // here, due to https://jira.codehaus.org/browse/JRUBY-6519
            ScriptingContainer container = new ScriptingContainer(LocalContextScope.CONCURRENT,
                    LocalVariableBehavior.TRANSIENT);
            container.setHomeDirectory("classpath:/META-INF/jruby.home");

            final boolean isDevMode = ConfluenceSystemProperties.isDevMode();
            container.setCompileMode(isDevMode ?
                    CompileMode.OFF :
                    CompileMode.JIT);
            container.setKCode(KCode.UTF8);

            // Define Ruby LOAD_PATH and add the Rubygems lib/ directories to it
            List<String> loadPaths = new ArrayList<String>(container.getLoadPaths());
            loadPaths.addAll(createLoadPaths(isDevMode));
            container.setLoadPaths(loadPaths);
            return container;
        }
    };

    private List<String> createLoadPaths(boolean isDevMode) {
        List<String> loadPaths = new ArrayList<String>();
        PathTransformer transformer = new ClassPathTransformer();
        if (isDevMode) {
            ResourcePathProvider resourcePathProvider = new ResourcePathProvider();
            transformer = new ResourceDirectoryTransformer(resourcePathProvider);
            // Add the ruby resource directory
            loadPaths.add(resourcePathProvider.getRubySourcesPath());
        }
        List<String> gemLoadPaths = resolveGemLoadPaths(transformer);
        loadPaths.addAll(gemLoadPaths);
        return loadPaths;
    }

    @Override
    public ScriptingContainer getScriptingContainer() {
        return engine.get();
    }

    private List<String> resolveGemLoadPaths(PathTransformer pathTransformer) {
        return Lists.transform(resolveGemlist(), pathTransformer);
    }

    @VisibleForTesting
    static class ResourcePathProvider {

        // We piggy back on the resource directories as used by the plugin framework
        private static final String PLUGIN_RESOURCE_DIRECTORIES = "plugin.resource.directories";

        private File rubySource;
        private File rubyGemsSource;

        ResourcePathProvider() {
            this(System.getProperty(PLUGIN_RESOURCE_DIRECTORIES));
        }

        ResourcePathProvider(String dirs) {
            if (StringUtils.isNotBlank(dirs)) {
                List<File> directories = Lists.transform(Arrays.asList(dirs.split(",")), new Function<String, File>() {
                    @Override public File apply(@Nullable String input) {
                        File file = new File(input);
                        if (file.exists()) {
                            return file;
                        }
                        return null;
                    }
                });

                rubySource = Iterables.find(directories, new Predicate<File>() {
                    @Override public boolean apply(@Nullable File input) {
                        return null != input && input.getAbsolutePath().endsWith("/ruby");
                    }
                }, null);

                rubyGemsSource = Iterables.find(directories, new Predicate<File>() {
                    @Override public boolean apply(@Nullable File input) {
                        return null != input && input.getAbsolutePath().endsWith("/rubygems/gems");
                    }
                }, null);
            }
        }

        String getRubySourcesPath() {
            return null != rubySource ? rubySource.getAbsolutePath() : null;
        }

        String getRubyGemsSourcesPath() {
            return null != rubyGemsSource ? rubyGemsSource.getAbsolutePath() : null;
        }
    }

    private static interface PathTransformer extends Function<String, String> {
    }

    private static class ResourceDirectoryTransformer implements PathTransformer {
        private final String rubyGemsPath;

        private ResourceDirectoryTransformer(ResourcePathProvider resourcePathProvider) {
            rubyGemsPath = resourcePathProvider.getRubyGemsSourcesPath();
        }

        @Override
        public String apply(@Nullable String s) {
            if (StringUtils.isNotEmpty(s)) {
                return String.format("%s/%s/lib", rubyGemsPath, s);
            }
            return null;
        }
    }

    private static class ClassPathTransformer implements PathTransformer {
        @Override
        public String apply(@Nullable String s) {
            if (StringUtils.isNotEmpty(s)) {
                return "classpath:/rubygems/gems/" + s + "/lib";
            }
            return null;
        }
    }

    private List<String> resolveGemlist() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("/META-INF/gemspec.index");
        try {
            return IOUtils.readLines(is);
        }
        catch (IOException e) {
            return Collections.emptyList();
        }
        finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Override
    public void destroy() throws Exception {
        getScriptingContainer().terminate();
    }

}
