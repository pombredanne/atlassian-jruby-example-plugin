package com.atlassian.plugins.polyglot.jrubyexample.servlet;

import java.util.List;
import java.util.Map;

/**
 * Define an interface to allow Ruby objects to be used in Java in a meaningful way.
 *
 * The Ruby class needs to "implement" the interface by including it as a module:
 * <pre>
 *     require 'java'
 *
 *     class RurlResponse
 *       include Java::com.atlassian.plugins.polyglot.jrubyexample.servlet.HttpResponseInformation
 *       ...
 *     end
 * </pre>
 */
public interface HttpResponseInformation {
    Map<String,String> getHttpHeaders();
    List<String> getBodyLines();
    List<String> getHeadlines();
    String getTitle();
}
